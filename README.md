# bitwarden-bin

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

A secure and free password manager for all of your devices.

https://bitwarden.com

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/bitwarden-bin.git
```


